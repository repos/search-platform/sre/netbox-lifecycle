#!/usr/bin/env python3
# SPDX-License-Identifier: Apache-2.0
# netbox-lifecycle.py
# given a server name prefix (as shown at https://wikitech.wikimedia.org/wiki/SRE/Infrastructure_naming_conventions#Servers)
# print a report of physical servers arranged by purchase date, and emphasis which hosts
# need to be rotated

import logging
import plac
import pynetbox
import os
import sys

from datetime import datetime, timedelta, timezone
from pprint import pprint

NB_TOKEN = os.environ.get('NB_TOKEN')
NB_URL = "https://netbox.wikimedia.org"

def get_dates():
    today = datetime.now()
    utc_today = today.replace(tzinfo=timezone.utc)
    five_years_ago = utc_today - (timedelta(weeks=(5 * 52)))
    return utc_today, five_years_ago

def get_old_servers(nb, prefix, utc_today, five_years_ago):
    old_servers = []
    devices = nb.dcim.devices.filter(name__isw=prefix, status='active')
    for device in devices:
        try:
            if 'purchase_date' in device.custom_fields:
                purchase_date = datetime.strptime(device.custom_fields['purchase_date'], '%Y-%m-%d')
                purchase_date_utc = purchase_date.replace(tzinfo=timezone.utc)
        except:
            print ("Can't find 'purchase_date' field for comparison. Exiting...")
            sys.exit(1)
        if purchase_date_utc < five_years_ago:
            old_servers.append(device)
    return old_servers

@plac.annotations(
prefix=plac.Annotation("server prefix to use")
)
def main(prefix):
    nb = pynetbox.api(NB_URL, token=NB_TOKEN)
    utc_today, five_years_ago = get_dates()
    old_servers = get_old_servers(nb, prefix, utc_today, five_years_ago)
    if len(old_servers) == 0:
        print (f"No servers matching the prefix {prefix} are older than 5 years.")
    else:
        pprint("The following servers need to be rotated: {}".format(old_servers))

if __name__ == '__main__':
    plac.call(main)
